dalekjs-travisci
================

Test project to test applications with DalekJS on Travis CI

**Status:** [![Build Status](https://travis-ci.org/vergissberlin/dalekjs-travisci.svg?branch=master)](https://travis-ci.org/vergissberlin/dalekjs-travisci)


## Tasks

1. Install node module from _packages.json_
- Start Webserver on Travis CI
- Run tests


**Note:** _This project is in development_