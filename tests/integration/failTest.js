/**
 * DalekJS on TravisCI
 *
 * @category Tests
 * @subcategory Integration
 * @package dalekjs-travisci
 * @author André Lademann <vergissberlin@googlemail.com>
 * @license https://andrelademann.de/license
 * @version 0.0.1
 */

module.exports = {
    'Fail test': function (test) {
        test
            .open('http://google.fr')
            .assert.title().is.not('Google', 'Title is correct.')
            .screenshot('report/dalek/screenshots/page-google.png')
            .done();
    }
};
