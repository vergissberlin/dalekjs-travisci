/**
 * DalekJS on TravisCI
 *
 * @category Tests
 * @subcategory Integration
 * @package dalekjs-travisci
 * @author André Lademann <vergissberlin@googlemail.com>
 * @license https://andrelademann.de/license
 * @version 0.0.1
 */

module.exports = {
    'Page title': function (test) {
        test
            .open('http://127.0.0.1:99.0/www/index.html')
            .assert.title().is('DalekJs on TravisCi', 'Title is correct.')
            .screenshot('report/dalek/screenshots/page-home.png')
            .done();
    },

    'Headline': function (test) {
        test
            .open('http://127.0.0.1:99.0/www/index.html')
			.assert.text('#headline', 'Hello DalekJS & Travis CI', 'The headline is correct')
            .done();
    }
};
